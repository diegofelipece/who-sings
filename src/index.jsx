import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'

import configureStore from 'flux'
import { applicationInit } from 'flux/actions/application.actions'

import { createMemoryHistory } from 'history'
import * as serviceWorker from './serviceWorker'
import App from './app/App'

const browserHistory = createMemoryHistory()
const store = configureStore(browserHistory)

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={browserHistory}>
      <App history={browserHistory} />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root'),
)
store.dispatch(applicationInit())

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()

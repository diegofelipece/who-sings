import {
  takeEvery, put,
} from 'redux-saga/effects'

import { types as applicationActions } from 'flux/actions/application.actions'
import { fetchTracks } from 'flux/actions/tracks.actions'

function* onApplicationInit() {
  yield put(fetchTracks())
}

export default function* () {
  yield takeEvery(applicationActions.INIT, onApplicationInit)
}

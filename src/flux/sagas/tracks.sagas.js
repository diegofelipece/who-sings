import {
  takeEvery, call, put,
} from 'redux-saga/effects'

import { types as tracksActions, setTracks } from 'flux/actions/tracks.actions'
import { setArtists } from 'flux/actions/artists.actions'

import { getMostPopularTracks } from 'api/musicxmatch'

function* onFetchTracks() {
  const tracks = yield call(getMostPopularTracks)
  yield put(setTracks(tracks))

  const artists = Object.values(tracks)
    .reduce(
      (allArtists, { artist }) => ({
        ...allArtists,
        [artist.id]: {
          ...artist,
        },
      }),
      {},
    )
  yield put(setArtists(artists))
}

export default function* () {
  yield takeEvery(tracksActions.FETCH, onFetchTracks)
}

import { all, fork } from 'redux-saga/effects'
import applicationSagas from 'flux/sagas/application.sagas'
import tracksSagas from 'flux/sagas/tracks.sagas'
import gameSagas from 'flux/sagas/game.sagas'

const rootSaga = function* rootSaga() {
  yield all([
    fork(applicationSagas),
    fork(tracksSagas),
    fork(gameSagas),
  ])
}

export default rootSaga

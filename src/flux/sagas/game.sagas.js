import {
  takeEvery, select, put, call,
} from 'redux-saga/effects'

import { setTrackLyrics } from 'flux/actions/tracks.actions'
import { types as gameActions, setGameQuestions } from 'flux/actions/game.actions'

import { tracksSelectors, artistsSelectors } from 'flux/selectors'

import suffleArraySort from 'utils/suffleArraySort'

import { getTrackLyrics } from 'api/musicxmatch'

function* onGameSetup() {
  let questions = []

  const tracks = yield select(tracksSelectors.selectRandomTracks, 10)

  for (const track of Object.values(tracks)) {
    const artists = yield select(artistsSelectors.selectRandomArtists, { avoid: track.artist.id })

    let lyrics = yield select(tracksSelectors.selectTrackLyrics, track.id)
    if (!lyrics) {
      lyrics = yield call(getTrackLyrics, track.id)
      yield put(setTrackLyrics({ [track.id]: lyrics }))
    }

    questions = [
      ...questions,
      {
        track,
        options: suffleArraySort([
          ...artists,
          track.artist,
        ]),
        lyrics,
      },
    ]
  }

  // dispatch de setGameQuestions
  yield put(setGameQuestions(questions))
}

export default function* () {
  yield takeEvery(gameActions.SETUP, onGameSetup)
}

import players from './players.initialState'
import tracks from './tracks.initialState'
import artists from './artists.initialState'
import game from './game.initialState'
import application from './application.initialState'

export {
  players,
  tracks,
  artists,
  game,
  application,
}

export default {
  players,
  tracks,
  artists,
  game,
  // application,
}

import * as gameSelectors from './game.selectors'
import * as tracksSelectors from './tracks.selectors'
import * as artistsSelectors from './artists.selectors'

export {
  gameSelectors,
  tracksSelectors,
  artistsSelectors,
}

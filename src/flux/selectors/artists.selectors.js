import getRandomIndex from 'utils/getRandomIndex'

export const selectArtists = (state) => state.artists

export const selectRandomArtists = (state, { quantity = 2, avoid = null }) => {
  const artistsObject = Object.values(state.artists)

  let validEntry = 0
  let selectedArtists = {}
  while (validEntry < quantity) {
    const key = getRandomIndex(artistsObject.length)
    const thisArtist = artistsObject[key]

    if (!selectedArtists[thisArtist.id] && avoid !== thisArtist.id) {
      selectedArtists = {
        ...selectedArtists,
        [thisArtist.id]: { ...thisArtist },
      }
      validEntry += 1
    }
  }

  return Object.values(selectedArtists)
}

import getRandomIndex from 'utils/getRandomIndex'

export const selectTracks = (state) => state.tracks.list
export const selectTracksNumber = (state) => Object.keys(state.tracks.list).length

export const selectRandomTracks = (state, quantity = 10) => {
  const tracksObject = Object.values(state.tracks.list)

  let validEntry = 0
  let selectedTracks = {}
  while (validEntry < quantity) {
    const key = getRandomIndex(tracksObject.length)
    const thisTrack = tracksObject[key]

    if (!selectedTracks[thisTrack.id]) {
      selectedTracks = {
        ...selectedTracks,
        [thisTrack.id]: { ...thisTrack },
      }
      validEntry += 1
    }
  }

  return Object.values(selectedTracks)
}

export const selectTrackLyrics = (state, trackId) => state.tracks.lyrics[trackId]

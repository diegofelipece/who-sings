import { types } from 'flux/actions/game.actions'
import { game as initialState } from 'flux/initialState'

export default function (state = initialState, action) {
  switch (action.type) {
    case types.SET_QUESTIONS: {
      const { questions } = action.payload
      return {
        ...state,
        questions,
      }
    }
    default:
      return state
  }
}

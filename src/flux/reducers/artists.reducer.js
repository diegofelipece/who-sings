import { types } from 'flux/actions/artists.actions'
import { artists as initialState } from 'flux/initialState'

export default function (state = initialState, action) {
  switch (action.type) {
    case types.SET_ARTISTS: {
      const { artists } = action.payload
      return {
        ...state,
        ...artists,
      }
    }
    default:
      return state
  }
}

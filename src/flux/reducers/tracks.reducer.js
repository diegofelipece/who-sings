import { types } from 'flux/actions/tracks.actions'
import { tracks as initialState } from 'flux/initialState'

export default function (state = initialState, action) {
  switch (action.type) {
    case types.SET_TRACKS: {
      const { tracks } = action.payload
      return {
        ...state,
        list: {
          ...state.list,
          ...tracks,
        },
      }
    }
    case types.SET_LYRICS: {
      const { lyrics } = action.payload
      return {
        ...state,
        lyrics: {
          ...state.lyrics,
          ...lyrics,
        },
      }
    }
    default:
      return state
  }
}

import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'

import players from './players.reducer'
import tracks from './tracks.reducer'
import artists from './artists.reducer'
import game from './game.reducer'

const createRootReducer = (history) => combineReducers({
  router: connectRouter(history),
  players,
  tracks,
  artists,
  game,
})

export default createRootReducer

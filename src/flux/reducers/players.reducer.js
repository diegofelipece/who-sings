import { types } from 'flux/actions/players.actions'
import { players as initialState } from 'flux/initialState'

export default function (state = initialState, action) {
  switch (action.type) {
    case types.SET_PLAYER: {
      // const { player } = action.payload
      return { ...state }
    }
    default:
      return state
  }
}

export const types = {
  FETCH: 'TRACKS/FETCH',
  SET_TRACKS: 'TRACKS/SET_TRACKS',
  SET_LYRICS: 'TRACKS/SET_LYRICS',
}

export const fetchTracks = () => ({
  type: types.FETCH,
})

export const setTracks = (tracks) => ({
  type: types.SET_TRACKS,
  payload: { tracks },
})

export const setTrackLyrics = (lyrics) => ({
  type: types.SET_LYRICS,
  payload: { lyrics },
})

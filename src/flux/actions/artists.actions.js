export const types = {
  SET_ARTISTS: 'TRACKS/SET_ARTISTS',
}

export const setArtists = (artists) => ({
  type: types.SET_ARTISTS,
  payload: { artists },
})

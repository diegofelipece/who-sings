import application from './application.actions'
import artists from './artists.actions'
import game from './game.actions'
import players from './players.actions'
import tracks from './tracks.actions'

export default {
  application,
  artists,
  game,
  players,
  tracks,
}

export const types = {
  SET_PLAYERS: 'PLAYERS/SET_PLAYERS',
}

export const setPlayer = (player) => ({
  type: types.SET_PLAYER,
  payload: { player },
})

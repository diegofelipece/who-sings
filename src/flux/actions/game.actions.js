export const types = {
  SETUP: 'GAME/SETUP',
  SET_QUESTIONS: 'GAME/SET_QUESTIONS',
}

export const gameSetup = () => ({
  type: types.SETUP,
})

export const setGameQuestions = (questions) => ({
  type: types.SET_QUESTIONS,
  payload: { questions },
})

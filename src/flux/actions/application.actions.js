export const types = {
  INIT: 'APPLICATION/INIT',
}

export const applicationInit = () => ({
  type: types.INIT,
})

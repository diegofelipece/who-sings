import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { routerMiddleware } from 'connected-react-router'

import initialState from 'flux/initialState'
import createRootReducer from 'flux/reducers'
import rootSaga from 'flux/sagas'

const sagaMiddleware = createSagaMiddleware()

// https://github.com/zalmoxisus/redux-devtools-extension
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export default function configureStore(history) {
  const middlewares = applyMiddleware(routerMiddleware(history), sagaMiddleware)

  const store = createStore(
    createRootReducer(history),
    initialState,
    composeEnhancers(middlewares),
  )

  sagaMiddleware.run(rootSaga)

  return store
}

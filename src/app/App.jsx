import React from 'react'
import {
  Router,
  Switch,
  Route,
  // Link,
} from 'react-router-dom'
import PropTypes from 'prop-types'

import '../styles.scss'

import Home from 'app/routes/home.route'
import Player from 'app/routes/player.route'
import Question from 'app/routes/questions.route'

const App = ({ history }) => {
  return (
    <Router history={history}>
      <div className="App">
        <header className="App__header">
            Who sings?
        </header>
        <div className="App__body">
          <Switch>
            <Route component={Player} path="/player/:playerId" />
            <Route component={Question} path="/questions" />
            <Route component={Home} path="*" />
          </Switch>
        </div>
        {/* <footer className="App__footer">
          <Link to="/">Home</Link>
          <Link to="/questions">/questions</Link>
          <Link to="/player/1">/player</Link>
        </footer> */}
      </div>
    </Router>
  )
}

App.propTypes = {
  history: PropTypes.shape({}).isRequired,
}

/* icon credit:
 Icons made by <a href="https://www.flaticon.com/authors/eucalyp" title="Eucalyp">Eucalyp</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
*/

export default App

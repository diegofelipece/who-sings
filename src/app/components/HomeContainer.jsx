import React from 'react'
import { Container, Button } from 'nes-react'
// Inital page, with top 10 players and start button
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux'

import { tracksSelectors } from 'flux/selectors'

const HomeContainer = () => {
  const hasFetchedTracks = useSelector(tracksSelectors.selectTracksNumber)
  return (
    <Container>
      <div>
        Home
      </div>
      <div>
        { hasFetchedTracks ? (
          <Link to="/questions">
            <Button>Start a new game</Button>
          </Link>
        ) : 'wait until we load some data...'}
      </div>
    </Container>
  )
}

export default HomeContainer

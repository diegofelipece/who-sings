import React, { useState } from 'react'
import { Container, Button } from 'nes-react'
import { useSelector } from 'react-redux'

import { gameSelectors } from 'flux/selectors'

const QuestionsContainer = () => {
  const points = {
    right: 1200,
    wrong: -322,
  }

  const totalOfQuestions = 10
  const [currentQuestion, setCurrentQuestion] = useState(1)
  const [score, setScore] = useState(0)
  const questions = useSelector(gameSelectors.selectGameQuestions)

  const isGameOver = !(currentQuestion <= totalOfQuestions)

  const question = questions[currentQuestion - 1]
  if (!question && !isGameOver) return 'loading ...'
  return (
    <Container>
      {isGameOver ? (
        <div>
          {`Total Score: ${score}`}
        </div>
      ) : (
        <>
          <div>
            {`Question ${currentQuestion} of ${totalOfQuestions}`}
          </div>
          <div>
            <h2>{question.lyrics}</h2>
          </div>
          {question.options.map((option) => (
            <div key={option.id}>
              <Button onClick={() => {
                setScore(
                  score + ((option.id === question.track.artist.id) ? points.right : points.wrong)
                )
                setCurrentQuestion(currentQuestion + 1)
              }}
              >
                {option.name}
              </Button>
            </div>
          ))}
        </>
      )}
    </Container>
  )
}

export default QuestionsContainer

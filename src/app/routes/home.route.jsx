import React from 'react'

import HomeContainer from 'app/components/HomeContainer'

export default () => <HomeContainer />

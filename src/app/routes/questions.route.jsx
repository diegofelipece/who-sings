import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import QuestionsContainer from 'app/components/QuestionsContainer'

import { gameSetup } from 'flux/actions/game.actions'

export default () => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(gameSetup())
  })

  return (<QuestionsContainer />)
}

import qs from 'qs'

const { REACT_APP_MUSICXMATCH_API_KEY: MUSICXMATCH_API_KEY } = process.env

const createApiUrl = (path, params) => {
  if (!MUSICXMATCH_API_KEY) throw new Error('You need to set an API key on .env file.')

  const encodedParams = qs.stringify({
    ...params,
    apikey: MUSICXMATCH_API_KEY,
  })

  return `/ws/1.1/${path}?${encodedParams}`
}

export const getMostPopularTracks = () => {
  const apiUrl = createApiUrl(
    'chart.tracks.get',
    {
      chart_name: 'top',
      page: 1,
      page_size: 100,
      country: 'it',
      f_has_lyrics: 1,
    },
  )

  return fetch(apiUrl)
    .then((res) => res.json())
    .then(({ message }) => message.body.track_list)
    .then((tracks) => tracks.reduce(
      (allTracks, { track }) => ({
        ...allTracks,
        [track.track_id]: {
          id: track.track_id,
          name: track.track_name,
          artist: {
            id: track.artist_id,
            name: track.artist_name,
          },
        },
      }),
      {},
    ))
    .catch((err) => console.error(err)) // eslint-disable-line
}

export const getTrackLyrics = (trackId) => {
  const apiUrl = createApiUrl(
    'track.lyrics.get',
    {
      track_id: trackId,
    },
  )

  return fetch(apiUrl)
    .then((res) => res.json())
    .then(({ message }) => message.body.lyrics.lyrics_body)
    .then((lyrics) => {
      const lyricsArray = lyrics
        .split('\n') // split line breacks
        .slice(0, -4) // remove API alerts
      return `${lyricsArray[0]} ... ${lyricsArray[1]}`
    })
    .catch((err) => console.error(err)) // eslint-disable-line
}
